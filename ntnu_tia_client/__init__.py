from .client import TiaClient
from .version import get_distribution


__all__ = ["TiaClient"]
__version__ = get_distribution().version
