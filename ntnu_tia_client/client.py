import enum
import logging
import urllib.parse
from types import ModuleType
from typing import Any, Dict, Optional, Tuple, Union

import requests

from .models import Person, KeyTypeEnum

logger = logging.getLogger(__name__)


class Response(enum.Enum):
    OK = enum.auto()
    UPDATED = enum.auto()
    CREATED = enum.auto()
    ILLEGAL_NAME = enum.auto()
    NOT_FOUND = enum.auto()
    ERROR = enum.auto()


class Visibility(enum.Enum):
    ALL = "all"
    INTERNAL = "internal"
    NONE = "none"


def quote_path_arg(arg: str) -> str:
    return urllib.parse.quote_plus(str(arg))


def urljoin(base_url: str, *paths: str) -> str:
    """An urljoin that takes multiple path arguments.

    Note how urllib.parse.urljoin will assume 'relative to parent' when the
    base_url doesn't end with a '/':

    >>> urllib.parse.urljoin('https://localhost/foo', 'bar')
    'https://localhost/bar'

    >>> urljoin('https://localhost/foo', 'bar')
    'https://localhost/foo/bar'

    >>> urljoin('https://localhost/foo', 'bar', 'baz')
    'https://localhost/foo/bar/baz'
    """
    for path in paths:
        base_url = urllib.parse.urljoin(base_url + "/", path)
    return base_url


def merge_dicts(*dicts: Optional[Dict[Any, Any]]) -> Dict[Any, Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class TiaEndpoints:
    def __init__(self, url: str):
        self.baseurl = url

    def __repr__(self) -> str:
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    def get_bas_person_url(
        self, keytype: KeyTypeEnum, keyvalue: str, query: Optional[Dict[str, Any]] = None
    ) -> str:
        if query is None:
            query = {
                "fields": "person.bas2_person",
            }
        return urljoin(
            self.baseurl,
            "person",
            quote_path_arg(keytype),
            quote_path_arg(keyvalue),
            "?" + urllib.parse.urlencode(query),
        )


class TiaClient:
    default_headers = {
        "Accept": "application/json",
    }

    def __init__(
        self,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        rewrite_url: Optional[Tuple[str, str]] = None,
        use_sessions: Union[bool, requests.Session] = True,
    ):
        self.urls = TiaEndpoints(url)
        self.headers = merge_dicts(self.default_headers, headers)
        self.rewrite_url = rewrite_url

        self.session: Union[requests.Session, ModuleType]
        if use_sessions:
            if isinstance(use_sessions, requests.Session):
                self.session = use_sessions
            else:
                self.session = requests.Session()
        else:
            self.session = requests

    def _call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        return_response: bool = False,
        auth: Optional[Tuple[str, str]] = None,
        **kwargs: Any,
    ) -> Optional[requests.Response]:
        headers = merge_dicts(self.headers, headers)
        if self.rewrite_url is not None:
            url = url.replace(*self.rewrite_url)
        response = self.session.request(
            method_name, url, auth=auth, headers=headers, **kwargs
        )
        if return_response:
            return response

        if response.status_code in (500, 400):
            logger.warn("Got HTTP %d: %r", response.status_code, response.json())
        if response.status_code == 404:
            return None
        response.raise_for_status()
        return response

    def get(self, path: str, **kwargs: Any) -> Optional[requests.Response]:
        return self._call("GET", path, **kwargs)

    def put(self, path: str, **kwargs: Any) -> Optional[requests.Response]:
        return self._call("PUT", path, **kwargs)

    def delete(self, path: str, **kwargs: Any) -> Optional[requests.Response]:
        return self._call("DELETE", path, **kwargs)

    def get_bas_person(
        self, keytype: KeyTypeEnum, keyvalue: str, query: Optional[Dict[str, Any]] = None
    ) -> Optional[Person]:
        url = self.urls.get_bas_person_url(keytype, keyvalue, query)
        response = self.get(url)
        if not response:
            return None
        data = response.json().get("person", {})
        if len(data) == 0 or "bas2_person" not in data:
            return None
        return Person.from_dict(data["bas2_person"])


def get_client(config_dict: Dict[str, Any]) -> TiaClient:
    """
    Get a TiaClient from configuration.
    """
    return TiaClient(
        config_dict["url"],
        headers=config_dict.get("headers"),
        rewrite_url=config_dict.get("rewrite_url"),
    )
