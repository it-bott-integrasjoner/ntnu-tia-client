import json
from enum import Enum
from typing import Any, Dict, List, Optional, Type, TypeVar, Union

import pydantic


T = TypeVar("T", bound="BaseModel")


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls: Type[T], data: Dict[str, Any]) -> T:
        return cls(**data)

    @classmethod
    def from_json(cls: Type[T], json_data: str) -> T:
        data = json.loads(json_data)
        return cls.from_dict(data)


class KontaktinformasjonTypeEnum(str, Enum):
    sip_address = "sip_address"
    email_work = "email_work"
    email_personal = "email_personal"


class KeyTypeEnum(str, Enum):
    """
    Person key types.

    To get a list of available keys: GET /person/keys
    """
    nin = "nin"
    ansattnr = "ansattnr"  # Paga ansattnr
    fslopenr = "fslopenr"
    dfo_ansatt_id = "dfo_ansatt_id"
    greg_person_id = "greg_person_id"
    feideid = "feideid"
    basid = "basid"
    email = "email"
    username = "username"
    bbuserid = "bbuserid"
    passnummer = "passnummer"
    studentnr = "studentnr"
    bilregemail = "bilregemail"
    personuuid = "personuuid"

    def __str__(self) -> str:
        return str(self.value)


class NameTypeEnum(str, Enum):
    displayname = "displayname"
    last_name = "last_name"
    first_name = "first_name"
    username = "username"


class StatusTypeEnum(str, Enum):
    personQuarantined = "personQuarantined"
    accountActivated = "accountActivated"
    reservePublish = "reservePublish"


class RoleTypeEnum(str, Enum):
    affiliated = "affiliated"
    oppdragstaker = "oppdragstaker"
    category = "category"
    arbeidstaker = "arbeidstaker"
    student = "student"


class Role(BaseModel):
    actsOn: Optional[Dict[str, str]]
    qualifier: Dict[str, str]
    _from: Optional[int]  # msEpoch
    to: Optional[int]  # msEpoch


class Person(BaseModel):
    lastUpdated: int  # msEpoch
    dateOfBirth: Optional[int]  # msEpoch
    gender: Optional[str]
    keys: Dict[Union[KeyTypeEnum, str], str]
    kontaktinformasjon: Optional[Dict[Union[KontaktinformasjonTypeEnum, str], str]]
    name: Optional[Dict[Union[NameTypeEnum, str], str]]
    roles: Optional[Dict[Union[RoleTypeEnum, str], List[Role]]]
    status: Optional[Dict[Union[StatusTypeEnum, str], str]]
