import pytest
import requests_mock


from ntnu_tia_client.client import TiaClient, TiaEndpoints


@pytest.fixture
def base_url():
    return "https://localhost"


@pytest.fixture
def client(base_url):
    return TiaClient(base_url)


@pytest.fixture
def endpoints(base_url):
    return TiaEndpoints(base_url)


@pytest.fixture
def mock_api(client):
    with requests_mock.Mocker() as m:
        yield m
