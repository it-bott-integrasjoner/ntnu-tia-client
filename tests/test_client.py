import pytest
import requests

from ntnu_tia_client.client import TiaClient


@pytest.mark.parametrize(
    argnames=["use_sessions"],
    argvalues=[
        [True],
        [False],
        [requests.Session()],
    ],
)
def test___init___use_sessions(base_url, use_sessions):
    client = TiaClient(
        url=base_url,
        use_sessions=use_sessions,
    )
    if use_sessions is True:
        assert isinstance(client.session, requests.Session)
    elif isinstance(use_sessions, requests.Session):
        assert client.session is use_sessions
    elif not use_sessions:
        assert client.session == requests
    else:
        assert False
