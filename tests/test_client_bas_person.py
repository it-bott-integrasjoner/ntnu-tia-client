import json

import pytest

from ntnu_tia_client import models
from ntnu_tia_client.models import KeyTypeEnum


@pytest.fixture
def bas_person_data(base_url):
    return json.loads(
        """
        {
          "person": {
            "bas2_person": {
              "lastUpdated": 1583335818619,
              "kontaktinformasjon": {
                "sip_address": "ola.normann123@example.com",
                "email_work": "ola.normann123@example.com"
              },
              "gender": "m",
              "keys": {
                "ansattnr": "10203040",
                "feideid": "olanormann@example.com",
                "basid": "12345",
                "email": "ola.normann123@example.com",
                "username": "olanormann",
                "nin": "01010112345",
                "fslopenr": "1234",
                "dfo_ansatt_id": "12345678",
                "greg_person_id": "12",
                "bbuserid": "111",
                "passnummer": "222",
                "studentnr": "333",
                "bilregemail": "444",
                "personuuid": "9226dc1a-6c43-496c-8dcb-71377af52ac0",
                "@@UNKNOWN_KEY@@": "value"
              },
              "roles": {
                "arbeidstaker": [
                  {
                    "qualifier": {
                      "position_category": "fast",
                      "bas_affiliation_source": "DFO_SAP",
                      "costcenter": "194167700"
                    },
                    "from": 1482793200000,
                    "actsOn": {
                      "ouid": "1208"
                    }
                  }
                ],
                "category": [
                  {
                    "qualifier": {
                      "position_category": "tekadm"
                    },
                    "from": 1581634800000,
                    "actsOn": {
                      "ouid": "1208"
                    }
                  }
                ]
              },
              "name": {
                "displayname": "Ola \\"The Viking\\" Normann",
                "last_name": "Normann",
                "first_name": "Ola",
                "username": "oldanormann"
              },
              "dateOfBirth": -2208988800000,
              "status": {
                "accountActivated": "J",
                "reservePublish": "N"
              }
            }
          },
          "api": {
            "path": "/V4",
            "vendor": "NTNU - IT Avdelingen",
            "deprecated": false,
            "name": "NTNU:TIA:REST",
            "version": "V4.0"
          }
        }
    """
    )


def test_get_bas_person(endpoints, client, mock_api, bas_person_data):
    mock_api.get(
        "https://localhost/person/username/olanormann/?fields=person.bas2_person",
        text=json.dumps(bas_person_data),
    )

    person = client.get_bas_person(KeyTypeEnum.username, "olanormann")

    assert person == models.Person.from_dict(
        bas_person_data["person"]["bas2_person"]
    )
    assert len(person.keys) == len(KeyTypeEnum) + 1
    for k in filter(lambda x: x != "@@UNKNOWN_KEY@@", person.keys):
        assert isinstance(k, KeyTypeEnum)
