ntnu-tia-client documentation
=============================

.. toctree::
   :titlesonly:
   :maxdepth: 2

   Introduction <intro>
   install

.. toctree::
   :caption: modules
   :maxdepth: 1
   :glob:

   modules/index
   modules/*

..   License <license>
..   Authors <authors>
..   Changelog <changes>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
