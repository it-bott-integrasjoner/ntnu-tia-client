Installing ntnu-tia-client
==========================

Install from repository
------------------------

TODO: Where will it be published?

Install from source
-------------------

Install in a `virtualenv`_ to avoid conflicts and other issues with your
operating system python environment:

::

   python3 -m venv /path/to/my_env
   source /path/to/my_env/bin/activate

Install *ntnu-tia-client* by running the included ``setup.py`` script:

::

   cd /path/to/ntnu_tia_client_source
   python setup.py install



.. _virtualenv: https://virtualenv.pypa.io/en/stable/
.. _PyPI: https://pypi.org/
.. _distutils config: https://docs.python.org/2.5/inst/config-syntax.html
.. _pip config: https://pip.pypa.io/en/stable/user_guide/#configuration
