# ntnu-tia-client

ntnu-tia-client is a Python library for accessing the [TIA api](tia-api).


## Usage

```python
from ntnu_tia_client import TiaClient

c = TiaClient('https://example.org', headers={'X-Gravitee-Api-Key': '<my-key>'})
person_data = c.get_bas_person('username', 'johndoe123')
```


 [tia-api]: https://api.ntnu.no/data/v4
